
library(tidyverse)
library(openxlsx)

###inlezen ARRA data
path <- "G:/OIS/Basisbestanden/arra/arraplus/RDS"

#inlezen vkp-bestanden locatus
temp = list.files(path= path, pattern="*.RDS")
myfiles <- map(file.path(path, temp), readRDS)

colsel <- c("vestnum","zaaknaam","gemcode","wp_tt_fp", "wpklas",  "brtk15",
          "straat", "huisnr","postcode","plaats","activenq","jaar")

#subsetting columns 
#myfiles <- lapply(myfiles, function(x) x[,.colsel])

#zelfde:
myfiles <- map(myfiles, ~select(.x, colsel))

#verbinden columns tot een dataframe
arra_def <- map_df(myfiles, bind_rows)


#maak stadsdelen
arra_def$sd15 <- str_sub(arra_def$brtk15,1,1)

#maak wijken
arra_def$bc15 <- str_sub(arra_def$brtk15,1,3)

#toevoegen 0 aan sbicode
arra_def$activenq2  <- str_pad(arra_def$activenq , 5, side="right", pad="0")

### --- koppelbestanden --- ###

df_crea <- read.xlsx("definities ABR.xlsx", sheet = "creatief")
df_ict <- read.xlsx("definities ABR.xlsx", sheet = "ict")
df_detailhandel <- read.xlsx("definities ABR.xlsx", sheet = "detailhandel")
df_horeca <- read.xlsx("definities ABR.xlsx", sheet = "horeca")
df_lifescience <- read.xlsx("definities ABR.xlsx", sheet = "lifescience")
df_rondvaart <- read.xlsx("definities ABR.xlsx", sheet = "rondvaart")

df_ict <- df_ict %>% 
  add_column(categorie = "ICT/tech-sector")

df_detailhandel <- df_detailhandel%>% 
  select(sbicode, omschrijving, categorie)

df_horeca <- df_horeca%>%
  add_column(categorie = "horeca")%>%
  select(sbicode, omschrijving, categorie)

df_lifescience <- df_lifescience%>%
  add_column(categorie = "lifescience/health")%>%
  select(sbicode, omschrijving, categorie)

#totale koppelbestand
df_totaal <- bind_rows(df_crea,
                       df_ict, 
                       df_detailhandel, 
                       df_horeca, 
                       df_lifescience, 
                       df_rondvaart)


df_totaal2 <-df_totaal%>% 
  distinct( .keep_all= T   )

df_totaal2$sbicode<- as.character(df_totaal2$sbicode)

### --- einde koppelbestanden --- ###




#koppen sectoren aan arra
arra_def <- arra_def %>% 
  left_join(df_totaal2 , by = c("activenq2"= "sbicode"))%>%
  mutate(branche=
           case_when(is.na(categorie)~"overig",
                     TRUE ~ categorie))



arra_def <- arra_def %>%
  mutate(omschrijving_crea2=case_when(omschrijving=="media en entertainment film"~"film",
                                      omschrijving=="media en entertainment overig"~"M&E overig",
                                      omschrijving=="creatieve zakelijke diensten"~"creatieve diensten",
                                      omschrijving=="kunst en cultuur"~"kunst, cultuur",
                                      TRUE ~"overig",
                                      is.na(omschrijving)~"overig"))




crea_levels <- c(
  "film",
  "M&E overig",
  "creatieve diensten",
  "kunst, cultuur",
  "overig" )

arra_def$omschrijving_crea2 <- factor(arra_def$omschrijving_crea2, levels=(crea_levels))

arra_def<- arra_def %>% 
  mutate(wpklas2=case_when(wpklas == '00' ~ 'geen wp',
                           wpklas == '01'~	'alleen partime wp',
                           wpklas == '02'~	'1 fulltime wp',
                           wpklas == '03'~	'2-4 wp ',
                           wpklas == '04'~	'5-9 wp',
                           wpklas == '05'~	'10-19 wp',
                           wpklas == '06'~	'20-49 wp',
                           wpklas == '07'~	'50-99 wp',
                           wpklas == '08'~	'100-199 wp',
                           wpklas == '09'~	'200-499 wp',
                           wpklas == '10'~	'500-799 wp',
                           wpklas == '11'~	'800-999 wp',
                           wpklas == '12'~	'1000-4999 wp',
                           wpklas == '13'~	'5000 en meer wp' ))


wpklas_levels <- c(
  'geen wp',
  'alleen partime wp',
  '1 fulltime wp',
  '2-4 wp ',
  '5-9 wp',
  '10-19 wp',
  '20-49 wp',
  '50-99 wp',
  '100-199 wp',
  '200-499 wp',
  '500-799 wp',
  '800-999 wp',
  '1000-4999 wp',
  '5000 en meer wp' )

arra_def$wpklas2 <- factor(arra_def$wpklas2, levels= rev(wpklas_levels))

